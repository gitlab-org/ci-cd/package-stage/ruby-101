dial_book = {
  "new york" => "212",
  "new brunswick" => "732",
  "edison" => "908",
  "plainsboro" => "609",
  "san francisco" => "301",
  "miami" => "305",
  "palo alto" => "650",
  "evanston" => "847",
  "orlando" => "407",
  "lancaster" => "717"
}

# Get city names from the hash
def get_city_names(city)
  city.keys
end

#try inserting a method here that will capitalize city names
def capitalize_array(array)
  array.map do |element|
    element.to_s.capitalize
  end
end

def get_area_code(hash, key)
  hash[key]
end

# Execution flow
loop do
  puts "Would you like to look up an area code based on city name? (Y/N)"
  answer = gets.chomp.downcase
  break if answer != "y"
  city_names = get_city_names(dial_book)
  puts capitalize_array(city_names)


  puts "Which city are you interested in?"
  city_name = gets.chomp
  if dial_book.include?(city_name)
    puts "The area code for #{city_name} is #{get_area_code(dial_book, city_name)}" # this was confusing that you can use city_name as the key, even though it's not in the hash
  else
    puts "Sorry, I don't know that one"
  end
end
