# Add addition, division, subtraction to this
# Add mod operator to show the remainder

def multiply(first_num, second_num)
  first_num.to_f * second_num.to_f
end
def addition(first_num, second_num)
  first_num.to_f + second_num.to_f
end
def subtract(first_num, second_num)
  first_num.to_f - second_num.to_f
end
def divide(first_num, second_num)
  first_num.to_f / second_num.to_f
end
def modulus(first_num,second_num)
  first_num.to_f % second_num.to_f
end

puts "Simple calculator"
25.times { print "-"}
puts
puts "Enter the first number"
num_1 = gets.chomp
puts "Enter the second number"
num_2 = gets.chomp
puts "The first number multiplied by the second number is #{multiply(num_1, num_2)}"
puts "The first number plus the second number is #{addition(num_1, num_2)}"
puts "The first number minus the second number is #{subtract(num_1, num_2)}"
puts "The first number divided by the second number is #{divide(num_1, num_2)}"
puts "The remainder of the first number divided by the second number is #{modulus(num_1, num_2)}"
