module Authentication
  require 'bcrypt'
  puts "Loaded Authentication module"

  def self.create_hash_digest(password)
    BCrypt::Password.create(password)
  end

  def self.verify_hash_digest(password)
    BCrypt::Password.new(password)
  end

  def self.authenticate_user(username, password, user_list)
    user_list.each do |user|
      if user[:username] == username && verify_hash_digest(user[:password]) == password
        return user
      end
    end
    "Credentials not correct"
  end

  def self.secure_user_passwords(user_list)
    user_list.each do |user|
      user[:password] = create_hash_digest(user[:password])
    end
    user_list
  end
end