sample_hash = {'a' => 1, 'b' => 2, 'c' => 3}
my_details = {'name' => 'john', 'favcolor' => 'blue'}
another_hash = {a: 1, b: 2, c:3}

# p my_details['favcolor']
# p sample_hash['b']

# p sample_hash
# p another_hash
# p another_hash.keys
# p another_hash.values
# add element
another_hash[:e] = "New Value"
another_hash[:f] = "Another Value"

# delete key/value
another_hash.delete(:e)

# iterate over hash
# another_hash.each do |key, value|
#   puts "The class for key is #{key.class} and the value is #{value.class}"
# end

# iterate over hash
another_hash.each { |key, value| puts "The key is #{key}, the value is #{value}"}

# Select string only
puts another_hash.select { |k, v| v.is_a?(String)}

# delete strings
puts another_hash.each { |k, v| another_hash.delete(k) if v.is_a?(String) }
