# Notes
# If a variable is pointing to another variable it's actually pointing to the location in memory where that variable is pointing to
# From CLI 'irb' to run interactive ruby
# Single quotes do not allow you to manipulate strings
#

#sentence = "My name is Tyler Durden"
#p sentence

first_name = "Tyler"
last_name = "Durden"
#full_name = first_name + last_name
full_name = "#{first_name} #{last_name}"
#puts first_name + " " + last_name
#puts "My first name is #{first_name} and my last name is #{last_name}"
#blah
