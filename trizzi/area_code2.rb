dial_book = {
  "new york" => "212",
  "new brunswick" => "732",
  "edison" => "908",
  "plainsboro" => "609",
  "san francisco" => "301",
  "miami" => "305",
  "palo alto" => "650",
  "evanston" => "847",
  "orlando" => "407",
  "lancaster" => "717"
}

# Get city names from the hash
def get_city_names(city)
city.keys
end

# Get area code based on given hash and key
def get_area_code(somehash, key)
somehash[key]
end

# Execution flow
loop do
puts "Would you like to add a city and area code to the dictionary? Press y for yes."
answer = gets.chomp.downcase
break if answer != "y"
puts get_city_names(dial_book)
puts "Here is the list of our current dictionary"
puts "What city would you like to add?"
new_city = gets.chomp.downcase
puts "Nice! And what is the area code?"
new_area_code = gets.chomp.to_s
dial_book[new_city] = new_area_code
puts "Here is the updated dictionary:"
25.times { print "-" }
puts
dial_book.each { |somekey, somevalue| puts "#{somekey} -  #{somevalue}" }
end
