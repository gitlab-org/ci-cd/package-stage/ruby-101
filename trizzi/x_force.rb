heroes = [
          { name: "wolverine", strength: "adamantium skeleton", defense: "healing factor", weakness: "water" },
          { name: "spiderman", strength: "agility", defense: "spidey-sense", weakness: "open spaces" },
          { name: "gambit", strength: "explosives", defense: "none", weakness: "telepathic attacks" },
          { name: "storm", strength: "lightning", defense: "wind", weakness: "enclosed spaces" },
          { name: "deadpool", strength: "tactical weapon use", defense: "healing factor", weakness: "giant explosions" }
        ]

def x_force(name, list_of_heroes)
  list_of_heroes.each do |hero|
    if hero[:name] == name
      return hero
    end
  end
    "I'm sorry, I don't recognize that name"
end

puts "Welcome to Cerebro, you may enter a heroes name and view their attributes."
25.times { print "-" }
puts
puts "You may view only two heroes, so choose wisely."
25.times { print "-" }
puts

attempts = 0
while attempts < 2
  print "Choose your hero: "
  name = gets.chomp
  old_man = x_force(name, heroes)
  p old_man
  attempts +=1
end
print "I hope this information proves useful"
puts
