# Add addition, division, subtraction to this
# Add mod operator to show the remainder

def multiply(first_num, second_num)
  first_num.to_f * second_num.to_f
end
def addition(first_num, second_num)
  first_num.to_f + second_num.to_f
end
def subtract(first_num, second_num)
  first_num.to_f - second_num.to_f
end
def divide(first_num, second_num)
  first_num.to_f / second_num.to_f
end
def modulus(first_num,second_num)
  first_num.to_f % second_num.to_f
end

## This is where I interact with the user
def start_calculator
  puts "Simple calculator"
  25.times { print "-"}
  puts
  puts "Enter the first number"
  num_1 = gets.chomp
  puts "Enter the second number"
  num_2 = gets.chomp
  puts "What would you like to do?"
  puts "Press 1 to multiply, 2 to add, 3, to subtract, 4 to divide, or 5 to see the remainder"
  function = gets.chomp
  if function == "1"
    puts "You have chosen to multiply #{num_1} by #{num_2}."
    puts "The answer is #{multiply(num_1, num_2)}"
  elsif function == "2"
    puts "You have chosen to add #{num_1} to #{num_2}."
    puts "The answer is #{addition(num_1, num_2)}"
  elsif function == "3"
    puts "You have chosen to subtract #{num_1} from #{num_2}."
    puts "The answer is #{subtract(num_1, num_2)}"
  elsif function == "4"
    puts "You have chosen to divide #{num_1} by #{num_2}."
    puts "The answer is #{divide(num_1, num_2)}"
  elsif function == "5"
    puts "You have chosen to see the remainder of  #{num_1} divided by #{num_2}."
    puts "The answer is #{modulus(num_1, num_2)}"
  elsif function == "42"
    puts "You have grasped the meaning of all life, the universe and everything. Great job"
  else
    puts "Sorry, that is an invalid response and I haven't learned yet how to repeat the loop so you have to start from the beginning."
    start_calculator
  end
end
start_calculator
