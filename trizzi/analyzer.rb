# Homework assignment
# Program should prompt user for first name, last name and return full name, full name reversed and number of characters in your name (not counting the space between names
# When done copy it it to the Q & A section)
# Notes
## puts = print a value
## gets.chomp will prompt the user to fill in some information
## gsub(argument)will substitute one string for another

puts "What is your first name"
first_name = gets.chomp
puts "What is your last name"
last_name = gets.chomp
full_name = "#{first_name} #{last_name}"
no_space = full_name.gsub(' ', '')
p "Thank you! Your full name is #{full_name}"
p "Your full name reversed is #{full_name.reverse}"
p "Your name has #{no_space.length} characters in it"
