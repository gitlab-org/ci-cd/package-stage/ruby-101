def multiply(first_num, second_num)
  return first_num.to_f * second_num.to_f
end

def divide(first_num, second_num)
  return first_num.to_f / second_num.to_f
end

def add(first_num, second_num) 
  return first_num.to_f + second_num.to_f
end

def sub(first_num, second_num) 
  return first_num.to_f - second_num.to_f
end

def mod(first_num, second_num) 
  return first_num.to_f % second_num.to_f
end

puts "Simple Calculator"
25.times { print "-" }
puts

puts "Enter the first number"
first_num = gets.chomp

puts "Enter the second number"
second_num = gets.chomp

puts "Multiply: #{multiply(first_num, second_num)}"
puts "Divide:   #{divide(first_num, second_num)}"
puts "Add:      #{add(first_num, second_num)}"
puts "Sub:      #{sub(first_num, second_num)}"
puts "Mod:      #{mod(first_num, second_num)}"
