# Notes
# When you use gets.chomp, it will always be a string. So, if you want to use an integer you have to convert it
# Command / comments or uncomments code

puts "What is your first name"
first_name = gets.chomp
puts "What is your last name"
last_name = gets.chomp
p "Thank you! You said your name is #{first_name} #{last_name}"

# puts "Enter a number to multiple by 2"
# input = gets.chomp
# puts input.to_i * 2
