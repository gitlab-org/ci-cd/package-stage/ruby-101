# Ruby 101

This project is for the GitLab Ruby 101  class at GitLab. As a group, we are working through the Udemy course: https://www.udemy.com/the-complete-ruby-on-rails-developer-course. 

This project will store our various assignments and coding projects.