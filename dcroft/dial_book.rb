dial_book = {
  'newyork' => '212',
  'newbrunswick' => '732',
  'edison' => '908',
  'plainsboro' => '609',
  'sanfrancisco' => '301',
  'miami' => '305',
  'paloalto' => '650',
  'evanston' => '847',
  'orlando' => '407',
  'lancaster' => '717'
}

# Get city names from the hash
def get_city_names(somehash)
  somehash.keys
end

# Get area code based on given hash and key
def get_area_code(somehash, key)
  somehash[key]
end

# Execution flow
loop do
  print 'Do you want to look up an area code by city name? (Y/N):'
  answer = gets.chomp.downcase
  break if answer != 'y'
  puts
  puts get_city_names(dial_book)
  print "Enter the city name you want to look up: "
  city_name = gets.chomp.downcase
  puts
  if dial_book.include?(city_name)
    puts "The area code for #{city_name} is #{get_area_code(dial_book, city_name)}"
  else
    puts "The city #{city_name} is not found."
  end
  puts
end
