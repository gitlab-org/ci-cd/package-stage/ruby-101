# Authenticator Project

# Hash of user names and passwords
users = [
          { username: "wolverine", password: "password1" },
          { username: "spiderman", password: "password2" },
          { username: "gambit", password: "password3" },
          { username: "storm", password: "password4" },
          { username: "deadpool", password: "password5" }
        ]

# Iterates through each user and attempts to find a matching username/password
def auth_user(user_name, password, list_of_heroes)
  list_of_heroes.each do |record|
    if record[:username] == user_name && record[:password] == password
      return record
    end
  end
  "Credentials were not correct"
end

# Application
puts "Bienvenidos a Authenticator"
25.times { print "-" }
puts
puts "This program inputs username and password and returns it in a string if it was inputed correctly"
puts "Note: You only have three chances to enter the correct password."
25.times { print "-" }
puts
puts "You should know, my sole purpose is to succeed in authenticating your password. If you fail, I have failed to fulfill my purpose."
25.times { print "-" }
puts
attempts = 1
while attempts < 4
  puts "Please enter your username to authenticate your password."
  print "Username: "
  user_name = gets.chomp
  print "Password: "
  password = gets.chomp
  authentication = auth_user(user_name, password, users)
  puts authentication
  puts "Press x to quit or any other key to continue"
  input = gets.chomp.downcase
  break if input == "x"
  attempts += 1
end
puts "You have exceeded the number of attempts" if attempts = 4
