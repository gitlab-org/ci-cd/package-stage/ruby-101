require_relative 'authentication'

class Student
  attr_accessor :first_name, :last_name, :email, :username, :password

  def initialize(first_name, last_name, email, username, password)
    @first_name = first_name
    @last_name = last_name
    @email = email
    @username = username
    @password = Authentication.create_hash_digest(password)
  end

  def to_s
    "Name: #{@first_name} #{@last_name}
    Username: #{@username}
    email: #{@email}"
  end

end
