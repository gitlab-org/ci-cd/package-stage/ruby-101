# mocked user data
users = [
  { username: "jhampton", password: "123" },
  { username: "sarahghp", password: "abc" },
  { username: "nkipling", password: "nki" }
]

# Iterates through each user and attempts to find a matching username/password
def auth_user(username, password, list_of_users) 
  list_of_users.each do |user_record|
    if user_record[:username] == username && user_record[:password] == password
      return user_record
    end
  end
  return "User credentials were not correct"
end

# Console output for user
puts "Welcome to the authenticator"
25.times { print "=" } 
puts
puts "This program will take input from the user and compare the password"
puts "If password is correct, you will get back the user object"

# Prompts the user for username and password 
attempts = 1
while attempts < 4
  print "Username: "
  username = gets.chomp
  print "Password: "
  password = gets.chomp

  auth_response = auth_user(username, password, users)
  puts auth_response

  puts "Press n to quit or any other key to continue"
  input = gets.chomp.downcase
  break if (input == "n")

  attempts += 1
end

puts "You have exceeded the number of attempts" if attempts == 4
