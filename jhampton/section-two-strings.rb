first_name = "john"
last_name = "hampton"

# String Interpolation
puts "My first name is #{first_name}, my last name is #{last_name}"

# Playground
puts first_name.class
puts 10.class
# puts first_name.methods

puts first_name.reverse
puts first_name.empty?
