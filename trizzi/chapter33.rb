# Practice with Arrays
# Some helpful practice problems here: https://www.w3resource.com/ruby-exercises/array/index.php
# Practice problem: Build a program that will let the user select two numbers and then create a range and then array of those two numbers, shuffle the list, let them select a number and then return the position of that number

def array_fun
  puts "Fun with arrays"
  25.times { print "-"}
  puts
  puts "Enter any number"
  num_1 = gets.chomp.to_i
  puts "Enter a second number"
  num_2 = gets.chomp.to_i
  if num_1 < num_2
    range = num_1..num_2
    array = range.to_a.shuffle
    p array
    puts "Choose a number and I'll tell you if it exists in the array and what the index is"
    lucky_num = gets.chomp.to_i
    if array.include?(lucky_num)
      puts "That number is in the array at index # #{array.find_index(lucky_num)}"
    else
      puts "Sorry, that number is not in the array, better luck next time"
    end
  elsif num_1 > num_2
    range = num_2..num_1
    array = range.to_a.shuffle
    p array
    puts "Choose a number and I'll tell you if it exists in the array and what the index is"
    lucky_num = gets.chomp.to_i
    if array.include?(lucky_num)
      puts "That number is in the array at index # #{array.find_index(lucky_num)}"
    else
      puts "Sorry, that number is not in the array, better luck next time"
    end
  else
    puts "Please select two different numbers"
    array_fun
  end
end
array_fun
