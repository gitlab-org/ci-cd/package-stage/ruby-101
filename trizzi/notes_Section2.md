### Overview
This document is intended to capture my notes for section two of the [GitLab Ruby 101 course](https://www.udemy.com/the-complete-ruby-on-rails-developer-course/learn/lecture/3846682#questions).

### Working with Arrays
- You signify an array with an open and closed bracket.
- It can be anything, a string, integer, etc.
- It is indexed, starting at 0
- Powerful, and lot's of iterators and methods you can use

#### Create an array
- From a range: x = 1..100 and then x.to_a to convert it to an array
- Type it out ["x", "y", "z", "a"]
- %w: "type something and it will turn it into an array"

#### Methods
- puts: each item on a different line
- print: will print the array
- last: will give you the last item in the array.
- reverse: will reverse the items in an array
- length: will tell you the length of an array
- shuffle:
- << element: will add something to the end of the list
- first: will add an item to the top of the list
- append: will append something to the end of the list
- uniq: will remove duplicates from the list
- empty?: test if the list is empty
- include?(" "):check to see if something is included
- push:will add an item to the list
- pop: (LIFO - last in, first out) remove the last item that was put into the list and removes it
- join: takes all the elements of an array and turns them into a string
  - you can also do join:("-") to insert a joining mechanism
- split: split an array by something
- _ grabs the last expression and sets it as a value
- capitalize: capitalize each item in the array

#### Comparison Operators (Works on numbers or strings)
- ==: returns true if both objects can be considered the same
- equal?: check if two objects are the same object
- <: less than
- <=: less than or equal
- >: greater than >
- >=: greater than or equal

#### Iterators
- The preferred way to iterate through data or loops is the .each operator.
  - `z.each {|foo| print foo + " "}`
- This let's you do whatever action you want to every item in the array
- select: works on boolean
  - `z.select {[name] number.odd?}`

### Things to remember
 - ! will mutate the caller
 - LIFO: Last in, first out
