def first
  puts "Enter your first name:"
  first_name = gets.chomp
  puts "Enter your last name:"
  last_name = gets.chomp

  full_name = "#{first_name} #{last_name}"

  puts "Your full name is #{full_name}."
  puts "Your full name reversed is #{full_name.reverse!}."
  puts "Your full name is #{full_name.reverse}."
  puts "Your full name is #{full_name}."
  puts "Your full name has #{first_name.length + last_name.length} characters in it."
end

def second
  puts "Simple Calculator"
  puts "-" * 20
  puts "What would you like to do? 1) multiply 2) divide 3) add 4) subtract 5) modulous"
  action = gets.chomp
  puts "Enter a number:"
  first_number = gets.chomp
  puts "Enter another number:"
  second_number = gets.chomp

  if action == '2' || action == 'divide'
    puts "Your first number divided by your second number: #{divide(first_number, second_number)}"
  elsif action == '3' || action == 'add'
    puts "Your first number added by your second number: #{add(first_number, second_number)}"
  elsif action == '4' || action == 'subtract'
    puts "Your first number subtracted by your second number: #{subtract(first_number, second_number)}"
  elsif action == '5' || action == 'modulous'
    puts "Your first number modulo by your second number: #{modulus(first_number, second_number)}"
  else
    puts "Your first number multiplied by your second number: #{multiply(first_number, second_number)}"
  end
end

def multiply(first, second)
  first.to_f * second.to_f
end

def divide(first, second)
  first.to_f / second.to_f
end

def add(first, second)
  first.to_f + second.to_f
end

def subtract(first, second)
  first.to_f - second.to_f
end

def modulus(first, second)
  first.to_f % second.to_f
end

def third
  users = [
    {username: "user1", password: "password"},
    {username: "user2", password: "password"},
    {username: "user3", password: "password"},
    {username: "user4", password: "password"},
    {username: "user5", password: "password"}
  ]

  puts "Simple Authenticator"
  puts "-" * 25
  puts "If you enter the correct username and password, the user object will be returned to you"
  puts
  
  attempts = 0

  while attempts < 3
    print "Enter username:"
    username = gets.chomp
    print "Enter password:"
    password = gets.chomp

    search_user = {username: username, password: password}

    success = users.select { |user| user == search_user }
    if !success.empty?
      return success[0]
    else
      puts "** Username or password incorrect"
    end

    puts
    attempts += 1
  end
end

third
